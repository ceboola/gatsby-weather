import Layout from '@components/Layout';

import 'semantic-ui-css/semantic.min.css';

export const wrapPageElement = ({ element }) => {
  return <Layout>{element}</Layout>;
};
