import styled from 'styled-components';
import { Link as LinkGatsby } from 'gatsby';

export const Header = styled.div`
  position: fixed;
  width: 100%;
  z-index: 1;
`;

export const Link = styled(LinkGatsby)`
  color: unset;
  &:hover {
    color: unset;
  }
`;

export const Wrapper = styled.div`
  position: fixed;
  right: 0;
  height: 60px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  z-index: 3;
  background-color: red;
`;
