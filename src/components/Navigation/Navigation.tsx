import { VFC } from 'react';

import { Wrapper } from './styled';

const Navigation: VFC<{ siteTitle: string }> = ({ siteTitle }) => {
  return <Wrapper>{siteTitle}</Wrapper>;
};

export default Navigation;
