import { VFC, memo } from 'react';
import { Container, List } from 'semantic-ui-react';

import { Segment } from './styled';

const Footer: VFC = () => {
  return (
    <Segment inverted vertical>
      <Container textAlign="center">
        <List horizontal inverted divided link size="small">
          <List.Item as="a" href="#">
            Site Map
          </List.Item>
          <List.Item as="a" href="#">
            Contact Us
          </List.Item>
          <List.Item as="a" href="#">
            Terms and Conditions
          </List.Item>
          <List.Item as="a" href="#">
            Privacy Policy
          </List.Item>
        </List>
      </Container>
    </Segment>
  );
};

export default memo(Footer);
