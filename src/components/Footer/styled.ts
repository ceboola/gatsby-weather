import styled from 'styled-components';
import { Segment as SegmentSemanticUI } from 'semantic-ui-react';

export const Segment = styled(SegmentSemanticUI)`
  display: flex;
  align-items: center;
  width: 100%;
  height: 70px;
`;
