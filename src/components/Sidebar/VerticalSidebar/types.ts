export interface VerticalSidebarProps {
  animation:
    | 'overlay'
    | 'push'
    | 'scale down'
    | 'uncover'
    | 'slide out'
    | 'slide along'
    | undefined;
  direction: 'top' | 'right' | 'bottom' | 'left';
  visible: boolean;
}
