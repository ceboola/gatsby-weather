import { VFC, memo } from 'react';
import { Link } from 'gatsby';
import { Icon, Menu, Sidebar } from 'semantic-ui-react';

import { VerticalSidebarProps } from './types';

const VerticalSidebar: VFC<VerticalSidebarProps> = ({
  animation,
  direction,
  visible,
}) => {
  return (
    <Sidebar
      as={Menu}
      animation={animation}
      direction={direction}
      icon="labeled"
      inverted
      vertical
      visible={visible}
      width="thin"
      style={{ position: 'fixed', paddingTop: '60px' }}
    >
      <Link to="/">
        <Menu.Item as="a">
          <Icon name="home" />
          Home
        </Menu.Item>
      </Link>
      <Link to="/page-2/">
        <Menu.Item as="a">
          <Icon name="gamepad" />
          page-2
        </Menu.Item>
      </Link>
      <Menu.Item as="a">
        <Icon name="camera" />
        Channels
      </Menu.Item>
    </Sidebar>
  );
};

export default memo(VerticalSidebar);
