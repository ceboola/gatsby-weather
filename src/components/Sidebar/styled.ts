import styled from 'styled-components';
import { Link } from 'gatsby';

export const Menu = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 78px;
`;

export const StyledLink = styled(Link)`
  width: 100%;
  text-align: center;
  color: #6fffb0;
  font-weight: 700;
  &:hover {
    color: #6fffb0;
  }
`;
