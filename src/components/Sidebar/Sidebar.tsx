import { useReducer, useEffect, VFC, memo, ReactNode } from 'react';
import Sticky from 'react-stickynode';
import {
  Button,
  Segment,
  Sidebar as SidebarSemanticUI,
} from 'semantic-ui-react';

import VerticalSidebar from '@components/Sidebar/VerticalSidebar';

const exampleReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_ANIMATION':
      return { ...state, animation: action.animation, visible: !state.visible };
    default:
      throw new Error();
  }
};

const Sidebar: VFC<{ content: ReactNode }> = ({ content }) => {
  const [state, dispatch] = useReducer(exampleReducer, {
    animation: 'overlay',
    direction: 'left',
    visible: false,
  });
  const { animation, direction, visible } = state;

  useEffect(() => {
    dispatch({ type: 'CHANGE_ANIMATION', animation: 'uncover' });
  }, []);

  return (
    <div>
      <SidebarSemanticUI.Pushable style={{ transform: 'none' }}>
        <VerticalSidebar
          animation={animation}
          direction={direction}
          visible={visible}
        />

        <SidebarSemanticUI.Pusher dimmed={false}>
          <Segment style={{ padding: '5em' }}>
            <Sticky>
              <Button
                onClick={() =>
                  dispatch({ type: 'CHANGE_ANIMATION', animation: 'uncover' })
                }
              >
                {!state.visible ? 'Open Menu' : 'Close Menu'}
              </Button>
            </Sticky>
            <Segment style={{ height: '100vh' }}>{content}</Segment>
          </Segment>
        </SidebarSemanticUI.Pusher>
      </SidebarSemanticUI.Pushable>
    </div>
  );
};

export default memo(Sidebar);
