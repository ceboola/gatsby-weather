import { FC } from 'react';
import { Grid } from 'semantic-ui-react';
import { graphql, useStaticQuery } from 'gatsby';

import Navigation from '@src/components/Navigation';
import Footer from '@components/Footer';
import Sidebar from '@components/Sidebar';

const Layout: FC = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);
  return (
    <Grid columns={1}>
      <Navigation siteTitle={data.site.siteMetadata?.title || `Title`} />
      <Grid.Column style={{ padding: '0' }}>
        <Sidebar content={children} />
      </Grid.Column>
      <Footer />
    </Grid>
  );
};

export default Layout;
