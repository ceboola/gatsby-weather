import { Segment } from 'semantic-ui-react';
import styled from 'styled-components';

export const Content = styled.div`
  display: flex;
  margin: 96px 0 32px 0;
  flex-direction: column;
`;

export const StyledSegment = styled(Segment)`
  padding: 0;
  margin: 0;
  border: none;
`;
